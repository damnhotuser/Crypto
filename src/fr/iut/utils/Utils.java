package fr.iut.utils;

/**
 * Created by Sydpy on 3/3/17.
 */
public class Utils {

    public static final double[] FR_FREQUENCIES = new double[]{0.0942,0.0102,0.0264,0.0339,0.1587,0.0095,0.0104,0.0077,0.0841,0.0089,0.00,0.0534,0.0324,0.0715,0.0514,0.0286,0.0106,0.0646,0.0790,0.0726,0.0624,0.0215,0.00,0.0030,0.0024,0.0032};
    public static final Double FR_IC = 0.078;

    public static Double computeIC(String s) {
        int[] counts = letterCounts(s);

        int textSize = s.length();

        double ic = 0.;

        for(int i = 0; i < 26; i++){
            ic += counts[i] * (counts[i] - 1);
        }

        if(textSize != 1) {
            ic /= (textSize * (textSize - 1));
        }

        return ic;
    }

    public static int[] letterCounts(String s) {
        int[] counts = new int[26];

        for(int i = 0 ; i < 26; i++){
            counts[i] = 0;
        }

        for (int i = 0; i < s.length(); i++) {
            counts[s.charAt(i) - 97] += 1;
        }

        return counts;
    }

    public static double[] frequence(String s) {

        int[] counts = letterCounts(s);

        double[] freq = new double[26];

        for(int i = 0; i < 26; i++){
            freq[i] = ((double) counts[i]) / s.length();
        }

        return freq;
    }

    public static int findRot(String s) {

        double[] freq = Utils.frequence(s);

        int minirot = 0;
        double minierr = 1000.;

        for(int rot = 0; rot < 26; rot++){
            double err = 0.;

            for(int i = 0; i < 26; i++){
                err += Math.abs(Math.abs(freq[(i+rot)%26] - FR_FREQUENCIES[i]));
            }

            if(err < minierr){
                minierr = err;
                minirot = rot;
            }
        }

        return minirot;
    }

}
