package fr.iut;

import fr.iut.caesar.Caesar;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextArea;

/**
 * Created by Martin on 02/03/2017.
 */

public class CaesarController {

    @FXML
    private TextArea intputField;

    @FXML
    private TextArea outputField;

    @FXML
    private Spinner keySpinner;

    public void encryptAction(ActionEvent e){
        String message = intputField.getText();
        int key = (int) keySpinner.getValue();

        intputField.setWrapText(true);

        outputField.setText(Caesar.Encrypt(message, key));
    }

    public void decryptAction(ActionEvent e){
        String message = outputField.getText();
        int key = (int) keySpinner.getValue();

        outputField.setText(Caesar.Decrypt(message, key));
    }

    public void crackAction(ActionEvent e){
        String encryptMessage = intputField.getText();
        outputField.setText(Caesar.Crack(encryptMessage));
    }
}
