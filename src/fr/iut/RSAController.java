package fr.iut;


import fr.iut.RSA.RSA;
import fr.iut.RSA.RSAKeyPair;
import fr.iut.RSA.RSAPublicKey;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.math.BigInteger;
import java.util.List;


public class RSAController {

    @FXML
    private ComboBox sizeSelector;

    @FXML
    private TextField hisE;

    @FXML
    private TextField hisN;

    @FXML
    private TextField myE;

    @FXML
    private TextField myD;

    @FXML
    private TextField myN;

    @FXML
    private TextArea cypherTextField;

    @FXML
    private TextArea decypherTextField;

    @FXML
    private TextField messageTextField;

    private RSAKeyPair myInfos;
    private RSAPublicKey hisInfos;

    @FXML public void generateKeys() {
        myInfos = RSAKeyPair.generateKeyPair((Integer) sizeSelector.getSelectionModel().getSelectedItem());

        myE.setText(myInfos.getPublicKey().e.toString());
        myN.setText(myInfos.getPublicKey().n.toString());
        myD.setText(myInfos.getPrivateKey().d.toString());
    }

    public void encrypt(MouseEvent mouseEvent) {

        String message = messageTextField.getText();

        if(message.isEmpty())
            messageTextField.setPromptText("Renseignez un message à crypter...");
        else {
            hisInfos = new RSAPublicKey();
            hisInfos.e = new BigInteger(hisE.getText());
            hisInfos.n = new BigInteger(hisN.getText());

            String cyphered = RSA.cypher(message, hisInfos);
            String cypheredChecksumed = cyphered + RSA.checksum(cyphered);
            String cypheredChecksumedSigned = RSA.sign(cypheredChecksumed, myInfos.getPrivateKey());

            cypherTextField.setText(cypheredChecksumedSigned);
        }
    }

    public void decrypt(MouseEvent mouseEvent) {
        String cyphered = cypherTextField.getText();

        if(!cyphered.isEmpty()){
            List<Byte> unsigned = RSA.unsign(cyphered, hisInfos);

            String unsignedVerified = RSA.verify(unsigned);
            if (unsignedVerified == null) {
                decypherTextField.setText("/!\\ LE CHECKSUM N'A PAS PU ETRE VERIFIE /!\\");
                return;
            }

            String unsignedVerifiedDecyphered = RSA.decypher(unsignedVerified, myInfos.getPrivateKey());
            decypherTextField.setText(unsignedVerifiedDecyphered);
        }

    }
}
