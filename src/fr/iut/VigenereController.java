package fr.iut;

import fr.iut.vigenere.Crack;
import fr.iut.vigenere.Vigenere;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.util.List;

/**
 * Created by Sydpy on 3/2/17.
 */
public class VigenereController {

    @FXML
    private TextArea textField;

    @FXML
    private TextField keyField;

    @FXML
    public TextArea resultField;

    public void cypherAction(ActionEvent event) {
        String text = textField.getText();
        String key = keyField.getText();

        String cyphered = Vigenere.cypher(text, key);

        resultField.setText(cyphered);
    }

    public void decypherAction(ActionEvent event) {
        String text = textField.getText();
        String key = keyField.getText();

        String decyphered = Vigenere.decypher(text, key);

        resultField.setText(decyphered);
    }

    public void crackAction(ActionEvent event) {
        String text = textField.getText();

        List<Crack> crack = Vigenere.crack(text);

        StringBuilder stringBuilder = new StringBuilder();
        if (crack != null) {
            crack.forEach(c -> {
                stringBuilder.append("\n\n==" + c.key + "======================================\n\n");
                stringBuilder.append(c.cracked);
            });
        }

        resultField.setText(stringBuilder.toString());
    }
}
