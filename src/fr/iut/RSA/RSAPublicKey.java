package fr.iut.RSA;

import java.math.BigInteger;

/**
 * Created by Sydpy on 3/3/17.
 */
public class RSAPublicKey {

    public BigInteger e;
    public BigInteger n;
}
