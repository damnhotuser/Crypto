package fr.iut.RSA;

import java.math.BigInteger;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sydpy on 3/3/17.
 */
public class RSA {

    private static int HASH_SIZE = 128;

    public static String cypher(String message, RSAPublicKey publicKey){
        message = Normalizer.normalize(message, Normalizer.Form.NFD);

        int rsaRcvrSize = publicKey.n.bitCount();

        byte[] bytes = message.getBytes();

        ArrayList<BigInteger> bigIntegers = new ArrayList<>();

        int step = rsaRcvrSize / 8 - 1;
        for (int i = 0; i < bytes.length; i += step) {
            byte[] tmp =
                    Arrays.copyOfRange(bytes, i,
                            Math.min(i + step, bytes.length));
            bigIntegers.add(new BigInteger(tmp)
                    .modPow(publicKey.e, publicKey.n));
        }

        StringBuilder cypheredMessage = new StringBuilder();
        bigIntegers.forEach(b -> {
            cypheredMessage.append(b);
            cypheredMessage.append("\n");
        });

        return cypheredMessage.toString();
    }

    public static String checksum(String message){
        return SHA512.hashText(message);
    }

    public static String sign(String message, RSAPrivateKey privateKey){

        RSAPublicKey fakePubKey = new RSAPublicKey();
        fakePubKey.n = privateKey.n;
        fakePubKey.e = privateKey.d;

        return cypher(message, fakePubKey);
    }

    public static List<Byte> unsign(String message, RSAPublicKey publicKey){
        String[] signedBigIntStrings = message.split("\n");

        List<BigInteger> unsignedBigInts = new ArrayList<>();
        for (String s : signedBigIntStrings) {
            BigInteger biggie = new BigInteger(s);
            biggie = biggie.modPow(publicKey.e, publicKey.n);
            unsignedBigInts.add(biggie);
        }

        List<Byte> unsignedBytes = new ArrayList<>();
        for (BigInteger b : unsignedBigInts) {
            byte[] bytes = b.toByteArray();
            for (byte aByte : bytes) {
                unsignedBytes.add(aByte);
            }
        }

        return unsignedBytes;
    }

    public static String verify(List<Byte> bytes) {

        StringBuilder sb = new StringBuilder();
        for(int i = bytes.size() - HASH_SIZE; i < bytes.size();i++)
            sb.append((char) bytes.get(i).intValue());

        String hash = sb.toString();

        bytes = bytes.subList(0,bytes.size() - HASH_SIZE);

        StringBuilder cypheredMessage = new StringBuilder();
        for (Byte b : bytes) {
            cypheredMessage.append((char) b.intValue());
        }

        String computedHash = SHA512.hashText(cypheredMessage.toString());

        if(!computedHash.equals(hash))
            return null;

        return cypheredMessage.toString();
    }

    public static String decypher(String message, RSAPrivateKey privateKey) {
        String[] split = message.split("\n");
        StringBuilder decypheredMessage = new StringBuilder();

        for (String s : split) {
            BigInteger biggie = new BigInteger(s);
            String str = new String(biggie.modPow(privateKey.d, privateKey.n).toByteArray());
            decypheredMessage.append(str);
        }

        return decypheredMessage.toString();
    }
}
