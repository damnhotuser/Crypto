package fr.iut.RSA;


import java.math.BigInteger;
import java.util.Random;

public class RSAKeyPair {

    private RSAPublicKey publicKey;
    private RSAPrivateKey privateKey;

    public static RSAKeyPair generateKeyPair(int size){
        return  new RSAKeyPair(size);
    }

    private static BigInteger nextRandomBigInteger(BigInteger n) {
        Random rand = new Random();
        BigInteger result = new BigInteger(n.bitLength(), rand);
        while( result.compareTo(n) >= 0 ) {
            result = new BigInteger(n.bitLength(), rand);
        }
        return result;
    }

    private static BigInteger findE(BigInteger f){
        BigInteger rand = nextRandomBigInteger(f);
        while(true){
            if(rand.gcd(f).equals(BigInteger.valueOf(1)))
                return rand;
            else
                rand = nextRandomBigInteger(f);
        }
    }

    public RSAKeyPair(RSAPublicKey publicKey, RSAPrivateKey privateKey){
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    private RSAKeyPair(int size) {
        Random rand = new Random();
        BigInteger p = new BigInteger(size, 100, rand);
        BigInteger q = new BigInteger(size, 100, rand);
        BigInteger phi = p.subtract(BigInteger.valueOf(1)).multiply(q.subtract(BigInteger.valueOf(1)));

        privateKey = new RSAPrivateKey();
        publicKey = new RSAPublicKey();

        publicKey.n = p.multiply(q);
        publicKey.e = findE(phi);
        privateKey.n = publicKey.n;
        privateKey.d = publicKey.e.modInverse(phi);
    }

    public RSAPublicKey getPublicKey() {
        return publicKey;
    }

    public RSAPrivateKey getPrivateKey() {
        return privateKey;
    }
}
