package fr.iut.vigenere;

import fr.iut.caesar.Caesar;
import fr.iut.utils.Utils;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sydpy on 3/2/17.
 */
public class Vigenere {

    private static final Double KEY_LENGTH_ERROR_THRESHOLD = 0.0005;
    private static int MAX_KEY_LENGTH = 1000000;

    public static String cypher(String plainText, String key){

        plainText = Normalizer.normalize(plainText, Normalizer.Form.NFD);
        plainText = plainText.replaceAll("[^a-zA-Z ]", "").toLowerCase().replace("\\s+", "");

        int keySize = key.length();
        int plainTextSize = plainText.length();

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i< plainTextSize; i++){
            sb.append(Character.toChars((plainText.charAt(i) + key.charAt(i%keySize) + 1) % 97 % 26 + 97));
        }

        return sb.toString();
    }

    public static String decypher(String cypherText, String key){

        cypherText = Normalizer.normalize(cypherText, Normalizer.Form.NFD);
        cypherText = cypherText.replaceAll("[^a-zA-Z ]", "").toLowerCase().replace("\\s+", "");

        int keySize = key.length();
        int plainTextSize = cypherText.length();

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i< plainTextSize; i++){
            sb.append(Caesar.Decrypt(Character.toString(cypherText.charAt(i)), ((int) key.charAt(i%keySize)) - 96));
        }

        return sb.toString();
    }

    public static List<Crack> crack(String cypherText){

        cypherText = Normalizer.normalize(cypherText, Normalizer.Form.NFD);
        cypherText = cypherText.replaceAll("[^a-zA-Z ]", "").toLowerCase().replace("\\s+", "");

        List<Crack> matches = new ArrayList<>();

        List<Integer> keyLengths = possibleKeyLengths(cypherText);

        String finalCypherText = cypherText;
        keyLengths.forEach(kl -> {
            matches.add(crack(finalCypherText, kl));
        });

        return matches;
    }

    private static Crack crack(String cypherText, Integer kl) {

        Crack crack = new Crack();

        StringBuilder key = new StringBuilder("");

        int cypherTextSize = cypherText.length();

        for(int i = 0; i< kl; i++){
            StringBuilder mi = new StringBuilder("");

            for(int j = i; j < cypherTextSize; j+=kl){
                mi.append(cypherText.charAt(j));
            }

            key.append(Character.toChars(Utils.findRot(mi.toString()) + 96));
        }

        crack.key = key.toString();
        crack.cracked = decypher(cypherText, key.toString());

        return crack;
    }

    private static List<Integer> possibleKeyLengths(String cypherText) {

        int cypherTextSize = cypherText.length();

        List<List<Double>> ics = new ArrayList<>();

        for(int l = 1; l < Math.min(MAX_KEY_LENGTH, cypherTextSize) + 1; l++){
            ics.add(new ArrayList<>());

            for(int i = 0; i < l; i++){
                StringBuilder mi = new StringBuilder("");

                for(int j = 0; j < cypherTextSize; j+=l){
                    mi.append(cypherText.charAt(j));
                }

                ics.get(l - 1).add(Utils.computeIC(mi.toString()));
            }
        }

        List<Double> icsaverages = new ArrayList<>();
        for(List<Double> list : ics){
            double avg = 0.;
            for(Double ic : list){
                avg += ic;
            }
            avg /= list.size();
            icsaverages.add(avg);
        }

        List<Double> errors = new ArrayList<>();
        for(Double d :icsaverages){
            errors.add(Math.pow(d - Utils.FR_IC, 2));
        }

        List<Integer> possibleKeyLengths = new ArrayList<>();
        for(int i = 0; i < errors.size(); i++){
            if(errors.get(i) < KEY_LENGTH_ERROR_THRESHOLD)
                possibleKeyLengths.add(i + 1);
        }

        return possibleKeyLengths;
    }


}
