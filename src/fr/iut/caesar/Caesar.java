package fr.iut.caesar;

import fr.iut.utils.Utils;
import java.text.Normalizer;

/**
 * Created by Martin on 02/03/2017.
 */
public class Caesar {

    public static String Encrypt(String message, int key){
        StringBuilder encryptMessage = new StringBuilder();
        key = key%26;

        message = message.toLowerCase();
        message = message.replaceAll("\\s+","");
        message = message.replaceAll("[:'-,;!?/_()=+*:]","");
        message = Normalizer.normalize(message, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");

        for (int i=0; i<message.length(); i++){
            if (message.charAt(i) + key <= 122)
                encryptMessage.append((char)(message.charAt(i) + key));
            else
                encryptMessage.append((char)(message.charAt(i) + key - 26));
        }
        return encryptMessage.toString();
    }

    public static String Decrypt(String message, int key){
        key = key%26;

        message = message.toLowerCase();
        message = message.replaceAll("\\s+","");
        message = message.replaceAll("[:'-,;!?/_()=+*:]","");
        message = Normalizer.normalize(message, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");

        StringBuilder decryptMessage = new StringBuilder();
        for (int i=0; i<message.length(); i++){
            if (message.charAt(i) - key >= 97)
                decryptMessage.append((char)(message.charAt(i) - key));
            else
                decryptMessage.append((char)(message.charAt(i) - key + 26));
        }
        return decryptMessage.toString();
    }

    public static String Crack(String message){
        return Decrypt(message, Utils.findRot(message));
    }
}
